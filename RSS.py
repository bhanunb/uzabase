import feedparser
import csv

class Input:
    """Parent Class"""
    def __init__(self):
        self.input = []
        self.output = []

    def cut(self):
        for _entry in self.input:
            result = self.cut_content(_entry)
            self.output.append(result)

    def convert(self):
        for _entry in self.input:
            result = self.convert_content(_entry)
            self.output.append(result)

    def cut_content(self,entry):
        """abstract method"""
        pass

    def convert_content(self,entry):
        """abstract method"""
        pass

    def standard_output(self):
        if len(self.output) == 0:
            raise ValueError('the out put is empty')
        header = ''
        for k in self.output[0].keys():
            header = header + '{:<12}|'.format(k)

        print(header)
        print('-'*len(header))
        for _out in self.output:
            row = ''
            for v in _out.values():
                row = row + '{:<12}|'.format(v)
            print(row)

    def file_output(self,path):
        if len(self.output) == 0:
            raise ValueError('the out put is empty')
        cols = list(self.output[0].keys())
        with open(path, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=cols)
            writer.writeheader()
            for data in self.output:
                writer.writerow(data)


class RSSInput(Input):
    def __init__(self,path):
        news_feed = feedparser.parse(path)
        self.input = news_feed.entries
        self.output = []

    def cut_content(self,entry):
        """I am overriding the parent class abstract method in this child class"""
        title = entry.title
        description = entry.description

        if len(title) > 10:
            title = title[:10]
        if len(description) > 10:
            description = description[:10]

        return {'title': title, 'description': description}

    def convert_content(self,entry):
        """I am overriding the parent class abstract method in this child class"""
        title = entry.title.replace('Uzabase','Uzabase, Inc')
        description = entry.description.replace('Uzabase','Uzabase, Inc')

        return {'title': title, 'description': description}


class FileInput(Input):
    def __init__(self,path):
        with open(path,'r') as f:
            rows = csv.DictReader(f)
            self.input = [dict(row) for row in rows]
        self.output = []

    def cut_content(self,entry):
        """I am overriding the parent class abstract method in this child class"""
        for k,v in entry.items():
            if len(v) > 10:
                entry[k] = v[:10]
        return entry

    def convert_content(self,entry):
        """I am overriding the parent class abstract method in this child class"""
        result_dict = {}
        for k, v in entry.items():
            result_dict[k] = v.replace('Uzabase','Uzabase, Inc')

        return result_dict
