import argparse,os

# Initialize parser
from RSS import FileInput, RSSInput


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", help="input rss url or csv file")
    parser.add_argument("-c", "--convert", help="cut or convert")
    parser.add_argument("-o", "--output", help="output file")

    # Read arguments from command line
    args = parser.parse_args()

    _input = args.input
    _convert = args.convert
    _output = args.output

    if _input:
        # This section below could have written as a factory method as well.
        # But I've used it like this as it's a simple logic in this program. 
        if os.path.exists(_input):
            input_obj = FileInput(_input)
        elif _input.startswith('http'):
            input_obj = RSSInput(_input)
        else:
            print('input should be either file or url')
            return

        if _convert:
            _convert_list = _convert.split(',')
            if 'cut' in _convert_list:
                input_obj.cut()
            if 'convert' in _convert_list:
                input_obj.convert()

        if _output:
            input_obj.file_output(_output)

        input_obj.standard_output()
    else:
        print('-i/--input is a mandatory argument')
        return

if __name__== "__main__" :
    main()
